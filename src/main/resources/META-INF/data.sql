-- Permissions
insert into "permission" values(uuid_generate_v4(), 'LIST_USERS');
insert into "permission" values(uuid_generate_v4(), 'DELETE_USERS');
insert into "permission" values(uuid_generate_v4(), 'MODIFY_USERS');
insert into "permission" values(uuid_generate_v4(), 'ADD_USERS');

-- Roles
insert into "role" values(uuid_generate_v4(), 'USER');
insert into "role" values(uuid_generate_v4(), 'ADMIN');

-- Mapping permissions to roles
-- User
insert into role_permission values((select id from "role" where "type" = 'USER'), (select id from "permission" where "type" = 'LIST_USERS'));

-- Admin
insert into role_permission values((select id from "role" where "type" = 'ADMIN'), (select id from "permission" where "type" = 'LIST_USERS'));
insert into role_permission values((select id from "role" where "type" = 'ADMIN'), (select id from "permission" where "type" = 'DELETE_USERS'));
insert into role_permission values((select id from "role" where "type" = 'ADMIN'), (select id from "permission" where "type" = 'MODIFY_USERS'));
insert into role_permission values((select id from "role" where "type" = 'ADMIN'), (select id from "permission" where "type" = 'ADD_USERS'));

-- Test users
insert into "user"(id, email, "password") values(uuid_generate_v4(), 'user@test.com', '$2a$10$4FACBVGl31tFa1FB7buabuhc/LNzzXPDE8LCGQs9yZzsKyRs4FZFi');
insert into "user"(id, email, "password") values(uuid_generate_v4(), 'admin@test.com', '$2a$10$ZjK/nk1R3FlNiSAhuZfWweMYUAv/s0N2KTFp/h/Jv9oc9V.zSO75q');

-- Mapping users to roles
insert into user_role values((select id from "user" where email = 'user@test.com'), (select id from "role" where "type" = 'USER'));
insert into user_role values((select id from "user" where email = 'admin@test.com'), (select id from "role" where "type" = 'ADMIN'));