package com.example.thorntail_rest.auth.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.example.thorntail_rest.shared.util.BaseEntity;
import com.example.thorntail_rest.user.entities.User;

@Entity
@Table(name = "token")
public class Token extends BaseEntity {

    @Column(nullable = false)
    private String value;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id")
    private User user;

    @Column(nullable = true, name = "expires_at")
    private Date expiresAt;

    public String getValue() {
        return this.value;
    }

    public User getUser() {
        return this.user;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Date getExpiresAt() {
        return expiresAt;
    }

    public void setExpiresAt(Date expiresAt) {
        this.expiresAt = expiresAt;
    }
}