package com.example.thorntail_rest.auth.entities;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.example.thorntail_rest.auth.util.RoleType;
import com.example.thorntail_rest.shared.util.BaseEntity;

@Entity
@Table(name = "role")
public class Role extends BaseEntity {

    @Column(nullable = false, unique = true)
    @Enumerated(EnumType.STRING)
    private RoleType type;

    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinTable(name = "role_permission", joinColumns = { @JoinColumn(name = "role_id") }, inverseJoinColumns = {
            @JoinColumn(name = "permission_id") })
    private Set<Permission> permissions = new HashSet<>();

    public RoleType getType() {
        return this.type;
    }

    public Set<Permission> getPermissions() {
        return this.permissions;
    }

    public void setType(RoleType type) {
        this.type = type;
    }

    public void setPermissions(Set<Permission> permissions) {
        this.permissions = permissions;
    }
}