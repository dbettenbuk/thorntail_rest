package com.example.thorntail_rest.auth.controllers;

import javax.annotation.security.PermitAll;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.example.thorntail_rest.auth.dtos.CredentialDto;
import com.example.thorntail_rest.auth.dtos.LoginResponse;
import com.example.thorntail_rest.auth.services.AuthService;
import com.example.thorntail_rest.auth.util.AuthenticatedUser;
import com.example.thorntail_rest.user.entities.User;

@Path("/auth")
public class AuthController {

    @Inject
    private AuthService authService;

    @Inject
    @AuthenticatedUser
    private User authenticatedUser;

    @POST
    @Path("login")
    @PermitAll
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response Login(CredentialDto dto) {
        LoginResponse response = this.authService.Login(dto.getEmail(), dto.getPassword());
        if (response != null) {
            return Response.ok(response).build();
        }

        return Response.status(Response.Status.NOT_FOUND).build();
    }

    @POST
    @Path("register")
    @PermitAll
    @Consumes(MediaType.APPLICATION_JSON)
    public Response Register(CredentialDto dto) {
        if (this.authService.Register(dto.getEmail(), dto.getPassword())) {
            return Response.ok().build();
        }

        return Response.status(Response.Status.BAD_REQUEST).build();
    }

    @GET
    @Path("logout")
    public Response Logout(@Context HttpHeaders headers) {
        String token = headers.getRequestHeader("Authorization").get(0).split(" ")[1];

        if (this.authService.logout(token)) {
            return Response.ok().build();
        }

        return Response.status(Response.Status.BAD_REQUEST).build();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response Authenticate() {
        if (this.authenticatedUser == null) {
            return Response.status(Response.Status.UNAUTHORIZED).build();
        }

        return Response.ok(this.authenticatedUser).build();
    }
}