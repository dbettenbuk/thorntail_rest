package com.example.thorntail_rest.auth.services;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import com.example.thorntail_rest.auth.dtos.LoginResponse;
import com.example.thorntail_rest.auth.entities.Token;
import com.example.thorntail_rest.auth.repositories.RoleRepository;
import com.example.thorntail_rest.auth.repositories.TokenRepository;
import com.example.thorntail_rest.auth.util.RoleType;
import com.example.thorntail_rest.user.entities.User;
import com.example.thorntail_rest.user.repositories.UserRepository;

import org.mindrot.jbcrypt.BCrypt;

@ApplicationScoped
public class AuthService {

    @Inject
    private UserRepository userRepository;

    @Inject
    private TokenRepository tokenRepository;

    @Inject
    private RoleRepository roleRepository;

    @Inject
    private Logger logger;

    public LoginResponse Login(String email, String password) {
        try {
            User user = this.userRepository.findByEmail(email);

            if (user != null && BCrypt.checkpw(password, user.getPassword())) {
                Token token = new Token();
                String tokenString = this.generateToken();
                token.setUser(user);
                token.setValue(tokenString);
                token.setExpiresAt(this.getExpiration());
                this.tokenRepository.saveTokenToUser(token, user);

                return new LoginResponse(user, tokenString);
            }

            return null;
        } catch (Exception exception) {
            this.logger.log(Level.WARNING, "Can not login user", exception);
            return null;
        }
    }

    public boolean Register(String email, String password) {
        try {
            User user = new User();
            user.setEmail(email);
            user.setPassword(BCrypt.hashpw(password, BCrypt.gensalt()));
            user.addRole(this.roleRepository.findByType(RoleType.USER));
            this.userRepository.merge(user);
            return true;
        } catch (Exception exception) {
            this.logger.log(Level.SEVERE, "Can not register user", exception);
            return false;
        }
    }

    public Token getTokenByValue(String value) {
        return this.tokenRepository.findByValue(value);
    }

    public boolean logout(String token) {
        return this.tokenRepository.removeByValue(token) == 1;
    }

    public boolean refreshToken(Token token) {
        token.setExpiresAt(this.getExpiration());
        return this.tokenRepository.merge(token) != null;
    }

    private String generateToken() {
        Random random = new SecureRandom();
        return new BigInteger(130, random).toString(32);
    }

    private Date getExpiration() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.DAY_OF_MONTH, 1);
        return calendar.getTime();
    }
}