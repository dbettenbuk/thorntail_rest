package com.example.thorntail_rest.auth.repositories;

import javax.enterprise.context.ApplicationScoped;

import com.example.thorntail_rest.auth.entities.Token;
import com.example.thorntail_rest.shared.util.BaseRepository;
import com.example.thorntail_rest.user.entities.User;

@ApplicationScoped
public class TokenRepository extends BaseRepository<Token> {

    public TokenRepository() {
        super(Token.class);
    }

    public Token findByValue(String value) {
        return this.getEntityManager().createQuery("SELECT t FROM Token t WHERE t.value = :value", Token.class)
                .setParameter("value", value).getSingleResult();
    }

    public void saveTokenToUser(Token token, User user) {
        user.getTokens().add(token);
        this.getEntityManager().merge(user);
    }

    public int removeByValue(String token) {
        return this.getEntityManager().createQuery("DELETE FROM Token t WHERE t.value = :value")
                .setParameter("value", token).executeUpdate();
    }
}