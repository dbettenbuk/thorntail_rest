package com.example.thorntail_rest.auth.repositories;

import javax.enterprise.context.ApplicationScoped;

import com.example.thorntail_rest.auth.entities.Role;
import com.example.thorntail_rest.auth.util.RoleType;
import com.example.thorntail_rest.shared.util.BaseRepository;

@ApplicationScoped
public class RoleRepository extends BaseRepository<Role> {

    public RoleRepository() {
        super(Role.class);
    }

    public Role findByType(RoleType type) {
        return this.getEntityManager().createQuery("SELECT r FROM Role r WHERE r.type = :type", Role.class)
                .setParameter("type", type).getSingleResult();
    }
}