package com.example.thorntail_rest.auth.repositories;

import java.util.Arrays;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;

import com.example.thorntail_rest.auth.entities.Permission;
import com.example.thorntail_rest.auth.util.PermissionType;
import com.example.thorntail_rest.shared.util.BaseRepository;

@ApplicationScoped
public class PermissionRepository extends BaseRepository<Permission> {

    public PermissionRepository() {
        super(Permission.class);
    }

    public List<Permission> findByTypes(PermissionType[] types) {
        return this.getEntityManager()
                .createQuery("SELECT p FROM Permission p WHERE p.type IN (:types)", Permission.class)
                .setParameter("types", Arrays.asList(types)).getResultList();
    }
}