package com.example.thorntail_rest.auth.util;

import java.util.Arrays;
import java.util.Optional;

public enum PermissionType {
    LIST_USERS("LIST_USERS"), ADD_USERS("ADD_USERS"), DELETE_USERS("DELETE_USERS"), MODIFY_USERS("MODIFY_USERS");

    private String name;

    public String getName() {
        return this.name;
    }

    private PermissionType(String name) {
        this.name = name;
    }

    public static PermissionType fromName(String name) {
        Optional<PermissionType> permissionType = Arrays.stream(PermissionType.values())
                .filter(item -> item.getName().equalsIgnoreCase(name)).findFirst();

        if (permissionType.isPresent()) {
            return permissionType.get();
        }

        throw new IllegalArgumentException(String.format("RoleType representation of %s was not found.", name));
    }
}