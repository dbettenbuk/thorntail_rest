package com.example.thorntail_rest.auth.util;

public final class HeaderUtils {

    public static String extractValueFromHeader(String headerString) {
        return headerString.split(":")[0];
    }

    public static String extractToken(String authorizationHeader) {
        return authorizationHeader.split(" ")[1];
    }
}