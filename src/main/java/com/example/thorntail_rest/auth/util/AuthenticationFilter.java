package com.example.thorntail_rest.auth.util;

import java.lang.reflect.Method;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.Priority;
import javax.annotation.security.DenyAll;
import javax.annotation.security.PermitAll;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;

import com.example.thorntail_rest.auth.entities.Token;
import com.example.thorntail_rest.auth.services.AuthService;
import com.example.thorntail_rest.user.entities.User;
import com.example.thorntail_rest.user.services.UserService;

@Provider
@Priority(Priorities.AUTHENTICATION)
public class AuthenticationFilter implements ContainerRequestFilter {

    @Context
    private ResourceInfo resourceInfo;

    @Inject
    private Logger logger;

    @Inject
    private UserService userService;

    @Inject
    private AuthService authService;

    @Inject
    @AuthenticatedUser
    private Event<String> userAuthenticated;

    @Override
    public void filter(ContainerRequestContext requestContext) {
        Method method = this.resourceInfo.getResourceMethod();

        if (!method.isAnnotationPresent(PermitAll.class)) {
            if (method.isAnnotationPresent(DenyAll.class)) {
                requestContext.abortWith(Response.status(Response.Status.UNAUTHORIZED)
                        .entity("Access to this resource is blocked for everyone").build());
                return;
            }

            String authHeader = requestContext.getHeaderString(HttpHeaders.AUTHORIZATION);
            String token = null;
            Token tokenEntity = null;

            try {
                token = HeaderUtils.extractToken(HeaderUtils.extractValueFromHeader(authHeader));
                tokenEntity = this.authService.getTokenByValue(token);
            } catch (Exception exception) {
                requestContext.abortWith(Response.status(Response.Status.UNAUTHORIZED).build());
                this.logger.log(Level.INFO, String.format("Unauthorized: Invalid token: %s", token));
                return;
            }

            User authenticatedUser = this.userService.findUserByToken(token);
            if (authenticatedUser == null || this.isTokenExpired(tokenEntity)) {
                this.logger.log(Level.INFO, String.format("Unauthorized: Invalid or expired token: %s", token));
                requestContext.abortWith(Response.status(Response.Status.UNAUTHORIZED).build());
                return;
            }

            if (!this.authService.refreshToken(tokenEntity)) {
                this.logger.log(Level.WARNING, "Could not refresh user token");
            }

            this.userAuthenticated.fire(token);
        }
    }

    private boolean isTokenExpired(Token token) {
        return token.getExpiresAt().before(new Date());
    }
}