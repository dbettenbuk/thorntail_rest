package com.example.thorntail_rest.auth.util;

import java.lang.reflect.Method;
import java.util.HashSet;
import java.util.Set;

import javax.annotation.Priority;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;

import com.example.thorntail_rest.auth.entities.Permission;
import com.example.thorntail_rest.auth.entities.Role;
import com.example.thorntail_rest.auth.repositories.PermissionRepository;
import com.example.thorntail_rest.user.entities.User;

@Provider
@RequestScoped
@Priority(Priorities.AUTHORIZATION)
public class PermissionFilter implements ContainerRequestFilter {

    @Context
    private ResourceInfo resourceInfo;

    @Inject
    @AuthenticatedUser
    private User authenticatedUser;

    @Inject
    private PermissionRepository permissionRepositry;

    @Override
    public void filter(ContainerRequestContext requestContext) {
        Method method = this.resourceInfo.getResourceMethod();

        if (method.isAnnotationPresent(Permissions.class)) {
            Permissions permissionsAnnotation = method.getAnnotation(Permissions.class);
            Set<Permission> permissions = new HashSet<>(
                    this.permissionRepositry.findByTypes(permissionsAnnotation.value()));
            Set<Role> userRoles = this.authenticatedUser.getRoles();
            boolean hasPermission = userRoles.stream()
                    .anyMatch((role) -> role.getPermissions().containsAll(permissions));

            if (!hasPermission) {
                requestContext.abortWith(Response.status(Response.Status.FORBIDDEN).build());
                return;
            }
        }
    }
}
