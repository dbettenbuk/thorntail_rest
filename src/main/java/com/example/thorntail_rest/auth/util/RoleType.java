package com.example.thorntail_rest.auth.util;

import java.util.Arrays;
import java.util.Optional;

public enum RoleType {
    USER("USER"), ADMIN("ADMIN");

    private String name;

    public String getName() {
        return this.name;
    }

    private RoleType(String name) {
        this.name = name;
    }

    public static RoleType fromName(String name) {
        Optional<RoleType> permissionType = Arrays.stream(RoleType.values())
                .filter(item -> item.getName().equalsIgnoreCase(name)).findFirst();

        if (permissionType.isPresent()) {
            return permissionType.get();
        }

        throw new IllegalArgumentException(String.format("RoleType representation of %s was not found.", name));
    }
}