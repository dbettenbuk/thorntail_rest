package com.example.thorntail_rest.shared.util;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

@Transactional
public abstract class BaseRepository<T extends BaseEntity> {

    @PersistenceContext
    private EntityManager entityManager;

    private Class<T> entityClass;

    protected EntityManager getEntityManager() {
        return this.entityManager;
    }

    protected BaseRepository(Class<T> entityClass) {
        this.entityClass = entityClass;
    }

    public List<T> findAll() {
        return this.entityManager
                .createQuery("Select e from " + this.entityClass.getSimpleName() + " e", this.entityClass)
                .getResultList();
    }

    public T findById(Object id) {
        return this.entityManager.find(entityClass, id);
    }

    public int removeById(Object id) {
        return this.entityManager.createQuery("DELETE FROM " + this.entityClass.getSimpleName() + " e WHERE e.id = :id")
                .setParameter("id", id).executeUpdate();
    }

    public T merge(T entity) {
        return this.entityManager.merge(entity);
    }

    public void persist(T entity) {
        this.entityManager.persist(entity);
    }
}