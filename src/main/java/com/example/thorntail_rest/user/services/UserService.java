package com.example.thorntail_rest.user.services;

import java.util.List;
import java.util.UUID;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import com.example.thorntail_rest.auth.repositories.RoleRepository;
import com.example.thorntail_rest.auth.util.RoleType;
import com.example.thorntail_rest.user.dtos.UserDto;
import com.example.thorntail_rest.user.entities.Address;
import com.example.thorntail_rest.user.entities.User;
import com.example.thorntail_rest.user.repositories.AddressRepository;
import com.example.thorntail_rest.user.repositories.UserRepository;

import org.mindrot.jbcrypt.BCrypt;

@ApplicationScoped
public class UserService {
    @Inject
    private UserRepository repository;

    @Inject
    private RoleRepository roleRepository;

    @Inject
    private AddressRepository addressRepository;

    public List<User> findAll() {
        return this.repository.findAll();
    }

    public User findById(String id) {
        return this.repository.findById(UUID.fromString(id));
    }

    public User findUserByToken(String token) {
        return this.repository.findByToken(token);
    }

    public boolean modifyUser(String id, UserDto userDto) {
        User user = this.repository.findById(UUID.fromString(id));

        if (user == null) {
            return false;
        }

        user.updateFromDto(userDto);

        return this.repository.merge(user) != null;
    }

    public void addUser(UserDto userDto) {
        User user = User.fromDto(userDto);
        user.setPassword(BCrypt.hashpw(userDto.getPassword(), BCrypt.gensalt()));
        user.addRole(this.roleRepository.findByType(RoleType.USER));
        this.repository.merge(user);
    }

    public boolean deleteUserById(String id) {
        UUID userId = UUID.fromString(id);
        User user = this.repository.findById(userId);
        user.getTokens().clear();
        this.repository.merge(user);
        int result = this.repository.removeById(userId);
        Address address = user.getAddress();
        if (address != null) {
            this.addressRepository.removeById(user.getAddress().getId());
        }
        return result == 1;
    }

    public boolean deleteUserAddress(String userId) {
        User user = this.repository.findById(UUID.fromString(userId));
        Address address = user.getAddress();
        user.setAddress(null);
        this.repository.merge(user);
        return this.addressRepository.removeById(address.getId()) == 1;
    }

    public boolean changePassword(User user, String newPassword) {
        user.setPassword(BCrypt.hashpw(newPassword, BCrypt.gensalt()));
        return this.repository.merge(user) != null;
    }

    public boolean passwordMatches(String rawPassword, String hashedPassword) {
        return BCrypt.checkpw(rawPassword, hashedPassword);
    }
}