package com.example.thorntail_rest.user.services;

import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import com.example.thorntail_rest.user.dtos.AddressDto;
import com.example.thorntail_rest.user.entities.Address;
import com.example.thorntail_rest.user.entities.User;
import com.example.thorntail_rest.user.repositories.AddressRepository;
import com.example.thorntail_rest.user.repositories.UserRepository;

@ApplicationScoped
public class AddressService {

    @Inject
    private AddressRepository addressRepository;

    @Inject
    private UserRepository userRepository;

    @Inject
    private Logger logger;

    public void addAddress(AddressDto addressDto) {
        this.addressRepository.persist(Address.fromDto(addressDto));
    }

    public List<Address> getAll() {
        return this.addressRepository.findAll();
    }

    public boolean update(String id, AddressDto addressDto) {
        Address address = this.addressRepository.findById(UUID.fromString(id));

        if (address == null) {
            return false;
        }

        address.updateFromDto(addressDto);
        return this.addressRepository.merge(address) != null;
    }

    public boolean deleteById(String id) {
        Address address = this.addressRepository.findById(UUID.fromString(id));
        try {
            User owner = this.userRepository.findByAddress(address);
            if (owner != null) {
                owner.setAddress(null);

                if (this.userRepository.merge(owner) != null) {
                    return true;
                }
            }
        } catch (Exception exception) {
            this.logger.log(Level.INFO, String.format("No owner for address with id: %s", id));
        }

        return this.addressRepository.removeById(address.getId()) == 1;
    }
}