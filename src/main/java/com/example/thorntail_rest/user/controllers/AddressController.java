package com.example.thorntail_rest.user.controllers;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.example.thorntail_rest.auth.util.PermissionType;
import com.example.thorntail_rest.auth.util.Permissions;
import com.example.thorntail_rest.user.dtos.AddressDto;
import com.example.thorntail_rest.user.services.AddressService;

@Path("/user/address")
public class AddressController {

    @Inject
    private AddressService addressService;

    @POST
    @Permissions({ PermissionType.ADD_USERS })
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addAddress(AddressDto addressDto) {
        if (!addressDto.isValid()) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        this.addressService.addAddress(addressDto);
        return Response.status(Response.Status.CREATED).build();
    }

    @GET
    @Permissions({ PermissionType.LIST_USERS })
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllAddresses() {
        return Response.ok(this.addressService.getAll()).build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("{id}")
    @Permissions({ PermissionType.MODIFY_USERS })
    public Response modifyAddress(@PathParam("id") String id, AddressDto addressDto) {
        if (!addressDto.isValid()) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }

        if (this.addressService.update(id, addressDto)) {
            return Response.ok().build();
        }

        return Response.status(Response.Status.BAD_REQUEST).build();
    }

    @DELETE
    @Path("{id}")
    @Permissions({ PermissionType.DELETE_USERS })
    public Response deleteAddress(@PathParam("id") String id) {
        if (this.addressService.deleteById(id)) {
            return Response.ok().build();
        }

        return Response.status(Response.Status.BAD_REQUEST).build();
    }

}