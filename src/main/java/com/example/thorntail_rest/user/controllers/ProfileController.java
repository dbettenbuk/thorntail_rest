package com.example.thorntail_rest.user.controllers;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.example.thorntail_rest.auth.util.AuthenticatedUser;
import com.example.thorntail_rest.user.dtos.PasswordChangeDto;
import com.example.thorntail_rest.user.dtos.UserDto;
import com.example.thorntail_rest.user.entities.User;
import com.example.thorntail_rest.user.services.UserService;

@Path("/user/profile")
public class ProfileController {
    @Inject
    private UserService userService;

    @Inject
    @AuthenticatedUser
    private User authenticatedUser;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getProfile() {
        return Response.ok(this.authenticatedUser).build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response modifyProfile(UserDto userDto) {
        if (this.userService.modifyUser(this.authenticatedUser.getId().toString(), userDto)) {
            return Response.ok().build();
        }

        return Response.status(Response.Status.BAD_REQUEST).build();
    }

    @POST
    @Path("password")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response changePassword(PasswordChangeDto passwordChangeDto) {
        if (!this.userService.passwordMatches(passwordChangeDto.getCurrentPassword(),
                this.authenticatedUser.getPassword())) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }

        if (this.userService.changePassword(this.authenticatedUser, passwordChangeDto.getNewPassword())) {
            return Response.ok().build();
        }

        return Response.status(Response.Status.BAD_REQUEST).build();
    }
}