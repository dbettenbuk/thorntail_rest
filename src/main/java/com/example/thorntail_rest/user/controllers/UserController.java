package com.example.thorntail_rest.user.controllers;

import javax.inject.Inject;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.example.thorntail_rest.auth.util.PermissionType;
import com.example.thorntail_rest.auth.util.Permissions;
import com.example.thorntail_rest.user.dtos.UserDto;
import com.example.thorntail_rest.user.entities.User;
import com.example.thorntail_rest.user.services.UserService;

@Path("/user")
public class UserController {

    @Inject
    private UserService userService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAll() {
        return Response.ok(this.userService.findAll()).build();
    }

    @DELETE
    @Path("{id}")
    @Permissions({ PermissionType.DELETE_USERS })
    public Response deleteUser(@PathParam("id") String id) {
        if (this.userService.deleteUserById(id)) {
            return Response.ok().build();
        }

        return Response.status(Response.Status.BAD_REQUEST).build();
    }

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getUser(@PathParam("id") String id) {
        User user = this.userService.findById(id);
        if (user != null) {
            return Response.ok(user).build();
        }

        return Response.status(Response.Status.BAD_REQUEST).build();
    }

    @POST
    @Path("{id}")
    @Permissions({ PermissionType.MODIFY_USERS })
    public Response modifyUser(@PathParam("id") String id, UserDto userDto) {
        if (userDto.getAddress() != null && !userDto.getAddress().isValid()) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }

        if (this.userService.modifyUser(id, userDto)) {
            return Response.ok().build();
        }

        return Response.status(Response.Status.BAD_REQUEST).build();
    }

    @POST
    @Permissions({ PermissionType.ADD_USERS })
    public Response addUser(UserDto userDto) {
        this.userService.addUser(userDto);
        return Response.status(Response.Status.CREATED).build();
    }
}