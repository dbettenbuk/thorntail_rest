package com.example.thorntail_rest.user.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.example.thorntail_rest.shared.util.BaseEntity;
import com.example.thorntail_rest.user.dtos.AddressDto;

@Entity
@Table(name = "address")
public class Address extends BaseEntity {

    @Column(nullable = false)
    private String city;

    @Column(nullable = false)
    private String streetAddress;

    public String getCity() {
        return this.city;
    }

    public String getStreetAddress() {
        return this.streetAddress;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setStreetAddress(String streetAddress) {
        this.streetAddress = streetAddress;
    }

    public void updateFromDto(AddressDto addressDto) {
        this.setCity(addressDto.getCity());
        this.setStreetAddress(addressDto.getStreetAddress());
    }

    public static Address fromDto(AddressDto addressDto) {
        if (addressDto == null) {
            return null;
        }
        Address address = new Address();
        address.city = addressDto.getCity();
        address.streetAddress = addressDto.getStreetAddress();
        return address;
    }
}