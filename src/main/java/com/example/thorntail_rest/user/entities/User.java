package com.example.thorntail_rest.user.entities;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.example.thorntail_rest.auth.entities.Role;
import com.example.thorntail_rest.auth.entities.Token;
import com.example.thorntail_rest.shared.util.BaseEntity;
import com.example.thorntail_rest.user.dtos.UserDto;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "\"user\"")
public class User extends BaseEntity {

    @Column(unique = true, nullable = false)
    private String email;

    @Column(nullable = false)
    @JsonIgnore
    private String password;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
    private Address address;

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
    @JsonIgnore
    private Set<Token> tokens = new HashSet<>();

    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinTable(name = "user_role", joinColumns = { @JoinColumn(name = "user_id") }, inverseJoinColumns = {
            @JoinColumn(name = "role_id") })
    private Set<Role> roles = new HashSet<>();

    public String getEmail() {
        return this.email;
    }

    public String getPassword() {
        return this.password;
    }

    public Address getAddress() {
        return this.address;
    }

    public Set<Role> getRoles() {
        return this.roles;
    }

    public Set<Token> getTokens() {
        return this.tokens;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public void setTokens(Set<Token> tokens) {
        this.tokens = tokens;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

    public void addRole(Role role) {
        this.roles.add(role);
    }

    public void updateFromDto(UserDto userDto) {
        this.email = userDto.getEmail();

        String password = userDto.getPassword();
        if (password != null && password.length() > 0) {
            this.password = userDto.getPassword();
        }

        this.address = Address.fromDto(userDto.getAddress());
    }

    public static User fromDto(UserDto userDto) {
        User user = new User();
        user.updateFromDto(userDto);
        return user;
    }
}