package com.example.thorntail_rest.user.repositories;

import javax.enterprise.context.ApplicationScoped;

import com.example.thorntail_rest.shared.util.BaseRepository;
import com.example.thorntail_rest.user.entities.Address;

@ApplicationScoped
public class AddressRepository extends BaseRepository<Address> {

    public AddressRepository() {
        super(Address.class);
    }
}