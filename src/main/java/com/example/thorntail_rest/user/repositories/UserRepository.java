package com.example.thorntail_rest.user.repositories;

import javax.enterprise.context.ApplicationScoped;

import com.example.thorntail_rest.shared.util.BaseRepository;
import com.example.thorntail_rest.user.entities.Address;
import com.example.thorntail_rest.user.entities.User;

@ApplicationScoped
public class UserRepository extends BaseRepository<User> {

    public UserRepository() {
        super(User.class);
    }

    public User findByEmail(String email) {
        return this.getEntityManager().createQuery("SELECT u FROM User u WHERE email = :email", User.class)
                .setParameter("email", email).getSingleResult();
    }

    public User findByToken(String token) {
        return this.getEntityManager()
                .createQuery("SELECT u FROM User u INNER JOIN u.tokens t WHERE t.value = :value", User.class)
                .setParameter("value", token).getSingleResult();
    }

    public User findByAddress(Address address) {
        return this.getEntityManager().createQuery("SELECT u FROM User u WHERE u.address = :address", User.class)
                .setParameter("address", address).getSingleResult();
    }
}